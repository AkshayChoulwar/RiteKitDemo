# Android Development Challenge (RiteKit)

The Main functionality of this app is it fetches the data provided by [https://api.ritekit.com/v1/search/trending?green=1&latin=1](https://api.ritekit.com/v1/search/trending?green=1&latin=1)
that is trending hashtags and also for each hashtag it shows it shows stats and influencers from [https://api.ritekit.com/v1/influencers/hashtag/hashtag](https://api.ritekit.com/v1/influencers/hashtag/hashtag).

# Procees of Implementation:
* Created MainActivity for Hashtags.
* Created StatsActivity for showing stats of hashtag.
* Created InfuencerActivity for showing influencers.

# Minimum Requirements:

* API 15
* Minimum SDK 4.0.3

# Libraries Used:

* Volley (Networking)
* Picasso (Image Loading)

# Screenshots of Application:
* Main Screen

![1__1_](/uploads/77179d86509e06e6dbdfdca1629ea8dc/1__1_.png)

* On Click on Influencer for Respective Hashtag.

![1__2_](/uploads/2a21eedb774dc1fa561cf871b6c41a5f/1__2_.png)

* On Click on stats for respective hashtag.

![1__3_](/uploads/9707e7880acc9ada53fb5343de8931ce/1__3_.png)

* On Scrolling.

![1__4_](/uploads/0f8b8080c1a411bf5efbe2dfea2fc7c9/1__4_.png)