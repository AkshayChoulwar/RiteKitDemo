package com.example.akshaychoulwar.ritekitdemo.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.akshaychoulwar.ritekitdemo.Activities.InfluenceActivity;
import com.example.akshaychoulwar.ritekitdemo.Activities.StatsActivity;
import com.example.akshaychoulwar.ritekitdemo.Models.HashModel;
import com.example.akshaychoulwar.ritekitdemo.R;

import java.util.List;

/**
 * Created by akshaychoulwar on 11/6/2017.
 */

public class HashAdapter extends RecyclerView.Adapter<HashAdapter.ViewHolder>
{
    private List<HashModel> hashModels;
    private Context context;

    public HashAdapter(List<HashModel> hashModels,Context context)
    {
        this.hashModels=hashModels;
        this.context=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.hash_list,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        final HashModel model=hashModels.get(position);
        holder.hash_name.setText(model.getTag());
    }


    @Override
    public int getItemCount() {
        return hashModels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView hash_name;
        private TextView influencers;
        private TextView stats;


        public ViewHolder(View itemview)
        {
            super(itemview);
            context=itemview.getContext();
            hash_name=(TextView) itemview.findViewById(R.id.tag_name);
            influencers=(TextView) itemview.findViewById(R.id.influencers);

            influencers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos=getAdapterPosition();
                    String pass=hashModels.get(pos).getTag();
                    Intent i=new Intent(context,InfluenceActivity.class);
                    i.putExtra("Hashtag",pass);
                    context.startActivity(i);
                }
            });

            stats=(TextView) itemview.findViewById(R.id.stats);
            stats.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos=getAdapterPosition();
                    int tweets=hashModels.get(pos).getTweets();
                    int retweets=hashModels.get(pos).getRetweets();
                    int exposure=hashModels.get(pos).getExposure();
                    int color=hashModels.get(pos).getColor();

                    Double links=hashModels.get(pos).getLinks();
                    Double mentions=hashModels.get(pos).getMentions();


                    String data=""+tweets+" "+retweets+" "+exposure+" "+color+" "+links+" "+mentions;

                    Intent i=new Intent(context, StatsActivity.class);
                    i.putExtra("Stats",data);
                    i.putExtra("Hashtag",hashModels.get(pos).getTag());
                    context.startActivity(i);
                }
            });
        }
    }
}
