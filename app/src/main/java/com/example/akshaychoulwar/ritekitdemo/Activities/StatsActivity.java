package com.example.akshaychoulwar.ritekitdemo.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.akshaychoulwar.ritekitdemo.R;

public class StatsActivity extends AppCompatActivity {

    private TextView tw;
    private TextView rtw;
    private TextView expo;
    private TextView col;
    private TextView links;
    private TextView mentions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Bundle bundle=getIntent().getExtras();
        String data=bundle.getString("Stats");
        String title=bundle.getString("Hashtag");
        setTitle(title);
        String dataarray[]=data.split("\\s+");

        tw=(TextView) findViewById(R.id.tw);
        rtw=(TextView) findViewById(R.id.rtw);
        expo=(TextView) findViewById(R.id.expo);
        col=(TextView) findViewById(R.id.col);
        links=(TextView) findViewById(R.id.links);
        mentions=(TextView) findViewById(R.id.mentions);


        String tweetscount=dataarray[0];
        String retweetscount=dataarray[1];
        String exposure=dataarray[2];
        String color=dataarray[3];

        Double no_of_links=Double.valueOf(dataarray[4]);
        Double no_of_mentions=Double.valueOf(dataarray[5]);


        int exposure_in_M=Integer.parseInt(dataarray[2])/1000000;
        int no_of_links_in_p= (int) (no_of_links*100);
        int no_of_mention_in_p= (int) (no_of_mentions*100);
        int int_color=Integer.parseInt(color);

        col.setTextColor(Color.parseColor("#FFFFFF"));
        tw.setTextColor(Color.parseColor("#FFFFFF"));
        rtw.setTextColor(Color.parseColor("#FFFFFF"));
        expo.setTextColor(Color.parseColor("#FFFFFF"));
        links.setTextColor(Color.parseColor("#FFFFFF"));
        mentions.setTextColor(Color.parseColor("#FFFFFF"));


        switch (int_color)
        {
            case 0:
                col.setBackgroundColor(Color.parseColor("#D50000"));
                col.setText("Unused");
                break;
            case 1:
                col.setBackgroundColor(Color.parseColor("#673AB7"));
                col.setText("Overused");
                break;
            case 2:
                col.setBackgroundColor(Color.parseColor("#8BC34A"));
                col.setText("Good");
                break;
            case 3:
                col.setBackgroundColor(Color.parseColor("#4CAF50"));
                col.setText("Great");
                break;
            default:
                col.setBackgroundColor(Color.parseColor("#FFFFFF"));
                col.setText("");
        }

        tw.setText("Tweets:"+tweetscount);
        rtw.setText("Retweets:"+retweetscount);
        expo.setText("Exposure:"+exposure_in_M+"M");
        links.setText("Links:"+no_of_links_in_p+"%");
        mentions.setText("Mentions:"+no_of_mention_in_p+"%");

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent=new Intent(getApplicationContext(),MainActivity.class);
        startActivityForResult(intent,0);
        return true;
    }

}
