package com.example.akshaychoulwar.ritekitdemo.Models;

/**
 * Created by akshaychoulwar on 11/6/2017.
 */

public class InfluenceModel {
    String imgurl;
    String username;
    int followers;

    public InfluenceModel(String imgurl,String username,int followers)
    {
        this.imgurl=imgurl;
        this.username=username;
        this.followers=followers;
    }

    public String getImgurl() {
        return imgurl;
    }

    public String getUsername() {
        return username;
    }

    public int getFollowers() {
        return followers;
    }
}
