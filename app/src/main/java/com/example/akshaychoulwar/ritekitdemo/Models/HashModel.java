package com.example.akshaychoulwar.ritekitdemo.Models;

/**
 * Created by akshaychoulwar on 11/6/2017.
 */

public class HashModel {
    String tag;
    int tweets;
    int retweets;
    int exposure;
    int color;
    Double links;
    Double mentions;

    public HashModel(String tag,int tweets,int retweets,int exposure,int color,Double links,Double mentions)
    {
        this.tag=tag;
        this.tweets=tweets;
        this.retweets=retweets;
        this.exposure=exposure;
        this.color=color;
        this.links=links;
        this.mentions=mentions;

    }

    public String getTag() {
        return tag;
    }

    public int getTweets() {
        return tweets;
    }

    public int getRetweets() {
        return retweets;
    }

    public int getExposure() {
        return exposure;
    }

    public int getColor() {
        return color;
    }

    public Double getLinks() {
        return links;
    }

    public Double getMentions() {
        return mentions;
    }


}
