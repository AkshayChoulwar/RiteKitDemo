package com.example.akshaychoulwar.ritekitdemo.Adapters;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.akshaychoulwar.ritekitdemo.Models.InfluenceModel;
import com.example.akshaychoulwar.ritekitdemo.R;
import com.squareup.picasso.Picasso;
import java.util.List;

/**
 * Created by akshaychoulwar on 11/6/2017.
 */

public class InfluenceAdapter extends RecyclerView.Adapter<InfluenceAdapter.ViewHolder>
{
    private List<InfluenceModel> influenceModels;
    private Context context;

    public InfluenceAdapter(List<InfluenceModel> influenceModels, Context context) {
        this.influenceModels = influenceModels;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.influence_list,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        final InfluenceModel model=influenceModels.get(position);
        Picasso.with(context).load(model.getImgurl()).fit().centerCrop().into(holder.photo);
        holder.username.setText("Username: "+model.getUsername());
        holder.followers.setText("Followers: "+model.getFollowers());


    }

    @Override
    public int getItemCount() {
        return influenceModels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder
    {
        private ImageView photo;
        private TextView username;
        private TextView followers;


        public ViewHolder(View itemview)
        {
            super(itemview);

            photo=(ImageView) itemview.findViewById(R.id.profile_id);
            username=(TextView) itemview.findViewById(R.id.user_id);
            followers=(TextView) itemview.findViewById(R.id.follow_id);
        }
    }

}
